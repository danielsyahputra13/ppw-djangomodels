from django.db import models

# Create your models here.
class Aktifitas(models.Model):
    deskripsi = models.TextField(max_length=300)
    nama_aktivitas = models.CharField(max_length=100)

    def __str__(self):
        return f"Aktifitas: {self.nama_aktivitas}"


class Member(models.Model):
    aktivitas = models.ForeignKey(Aktifitas, on_delete=models.CASCADE, related_name="member_aktivitas")
    nama_member = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.nama_member}"